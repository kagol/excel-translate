console.log('statistic~~');
const path = require('path');
const xlsx = require('node-xlsx');
const { getFeedbacks } = require('./util');

function stat(path, langConfig) {
  const sheetList = xlsx.parse(path);
  const tasks = getFeedbacks(sheetList);

  Promise.all(tasks)
  .then((values) => {
    console.log('values:', values.length);
    sheetList.forEach((sheet) => {
      sheet.data.slice(1).forEach((row, rowIndex) => {
        row.forEach((cell, cellIndex) => {
          if (cellIndex === 0) { // 第一列
            sheet.data[rowIndex+1][cellIndex+1] = values[rowIndex];
          }
        });
      });
    });
  })
  .then(() => {
    console.log('buildSheetList:', xlsx.build(sheetList));
  });
}

const file = path.resolve(__dirname, '../1.xlsx');
stat(file)
